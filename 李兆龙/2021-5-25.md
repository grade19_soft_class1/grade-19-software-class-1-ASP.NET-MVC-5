# Vue2.0的基础用法

## **声明式渲染**

```
HTML
<div id="app">                                  
  {{ information }}
</div>
```
   
```
JS
var app = new Vue({
  el: '#app',
  data: {
    message: 'No pain no gains!'
  }
})
```

<code>No pain no gains!</code>

## **除了文本插值，还可以这样绑定属性复含条件与循环**

```
HTML
 <div id="app">
        <p v-if="seen" v-bind:title="message">
            光标移动到此处有惊喜👈
        </p>
</div>
```

```
JS
 var app = new Vue({
        el: '#app',
        data: {
            message: new Date().toLocaleDateString(),
            seen: true
        },
    })
```
## **若在控制台输入`app.seen = false`,之前的消息就消失了**
   
![](./属性绑定.png)

## **`v-for`指令可以绑定数组的数据来渲染一个项目列表：**

```
HTML
 <div id="app-2">
        <ul>
            <div v-for="item in list" >
                {{item.id}} {{item.content}}    
            </div>
        </ul>
    </div>
```
   
```
JS
 var app2 = new Vue({
        el: '#app-2',
        data: {
            list: [
                {
                    id: 1,
                    content: "空无一人这片沙滩"
                },
                {
                    id: 2,
                    content: "风吹过来冷冷海岸"
                },
                {
                    id: 3,
                    content: "我轻轻抖落鞋里的沙"
                },
                {
                    id: 4,
                    content: "看着我的脚印"
                }
            ]
        }
    })    
```

<code>
1 空无一人这片沙滩
</br>
2 风吹过来冷冷海岸
</br>
3 我轻轻抖落鞋里的沙
</br>
4 看着我的脚印
</code>
---
   
## **Vue还提供了`v-model`指令，它能轻松实现表单输入和应用状态之间的双向绑定。**

```
HTML
<div id="app-3">
        <form>
            {{username}}
            <label for="">Username:</label><input type="text" v-model="DataBase.username">
            {{password}}
            <label for="">Password:</label><input type="password" v-model="DataBase.password">
            
        </form>
        {{str}}
    </div>
```
   
```
JS
 var app3 = new Vue({

        el: '#app-3',
        data: {
            DataBase: {
                username: '',
                password: ''
            }
        },
        methods: {
           
        },
        computed: {
                str: function () {
                    return `用户名:${this.DataBase.username},密码:${this.DataBase.password}`
                }
            }
    })
```

![](MODEL.png)