# 5.20日课堂笔记

## session 的定义

Session：在计算机中，尤其是在网络应用中，称为“会话控制”。Session对象存储特定用户会话所需的属性及配置信息。这样，当用户在应用程序的Web页之间跳转时，存储在Session对象中的变量将不会丢失，而是在整个用户会话中一直存在下去。当用户请求来自应用程序的 Web页时，如果该用户还没有会话，则Web服务器将自动创建一个 Session对象。当会话过期或被放弃后，服务器将终止该会话。Session 对象最常见的一个用法就是存储用户的首选项

## session 使用

首先，我们在登录成功后，创建一个session["username"]用来存放当前用户名，代码如下

```
 public string LoginDone(LoginModel loginModel)
        {
            var username = loginModel.Username.Trim();
            var password = loginModel.Password.Trim();
            dynamic req;
            //判断是否有这个用户
            var user = db.Users.Where(x => x.Username.Equals(username) && x.Password.Equals(password)).FirstOrDefault();
            if(user != null)
            {

                req = new
                {
                    code = 200,
                    msg = "登录成功"
                };
                Session["UserName"] = username;
            }
            else
            {
                req = new
                {
                    code = 1000,
                    msg = "用户名或密码不正确，请核对后重试"
                };
            }
            return JsonConvert.SerializeObject(req);
            
        }
```
在进入主页后，用session 判断session{”username“}是否为空，如果为空，直接返回登入路由，如果不为空，就进入主页

```
public ActionResult Index()
        {
            if (Session["UserName"] == null)
            {
                return View("~/Views/users/login.cshtml");
            }
        }
```