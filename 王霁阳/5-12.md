# 什么是asp.net

ASP.NET又称为ASP+，不仅仅是ASP的简单升级，而是微软公司推出的新一代脚本语言。ASP.NET基于.NET Framework的Web开发平台，不但吸收了ASP以前版本的最大优点并参照Java、VB语言的开发优势加入了许多新的特色，同时也修正了以前的ASP版本的运行错误。

优点如下：

1、是面向对象的编程语言，简单易学。

2、具有面向对象编程语言的一切特性，比如封装性、继承性、多态性等等,封装性使得代码逻辑清晰，并且应用到ASP.NET上就可以使业务逻辑和Html页面分离；继承性和多态性使得代码的可重用性大大提高

# 什么是mvc

MVC与ASP.NET完全没有关系，是一个全新的Web开发，事实上ASP.NET是创建WEB应用的框架而MVC是能够用更好的方法来组织并管理代码的一种更高级架构体系，所以可以称之为ASP.NET MVC。可将原来的ASP.NET称为 ASP.NET Webforms，新的MVC 称为ASP.NET MVC。


MVC是软件架构的一种模式，可以说就相当于常说的设计模式。其中主要包括三个模块，顾名思义。就如MVC名字所显示的那样：模型（Model），视图（View），控制器（Controller）。mvc中的模型（Model）和视图（View ）是完全区别于三层架构中的模型（Model）和视图（View）的。

# 今日所学知识

如果没有进行修改的话,这个路由默认访问的就是所引用的模板的首页,如果想要改变首页，那就改变App_Start文件夹下的RouteConfig.cs文件，文件如下
```
routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
```
在Mvc中有一个默认的路由 一般是分为三段 {controller}/{action}/{id} 

默认生成的是 home/index/id

他的值与上述相对应

在HomeController控制下修改完路由后就可直接访问你路由路径下的页面

```
return View("~/Views/Home/About.cshtml");
```
控制器中，返回视图的时候默认返回的是控制器同名的视图(忽略controller)的Views下的视图文件 例如：Views/Data/Index.cshtml

如果要返回不同的视图

1.如果是在控制器同名的视图目录，可以简写为details的形式
```
    return Views("xxx");
```
2.反之就要写完整的路径
```
    return Views("Views/Data/index.cshtml");