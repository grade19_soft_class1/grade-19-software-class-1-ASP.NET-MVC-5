# 5月15日课堂笔记

## 模型
首先，在models 目录下创建一个类
![图](./images/1.PNG)
![图](./images/2.PNG)
然后再添加一个包含视图的MVC5控制器，要注意的是，在添加之前先要重新生成解决方案

然后再把web.config 里的连接数据库的字段修改一下

```
<connectionStrings>
    <add name="ContextDB" connectionString="server =.; database=saysay; uid=sa; pwd=123456;" providerName="System.Data.SqlClient" />
  </connectionStrings>
```
完成
要注意的是，使用EF postgreSQL 数据库时 要把providerName的值改成Npgsql

还有一点，要在data文件夹里的文件添加
```
protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");
        }
```
因为SQL server 和postgreSQL创建的前缀不同，要不然创建不了表
## 课后练习

创建用户数据表的时候，自动创建一个用户