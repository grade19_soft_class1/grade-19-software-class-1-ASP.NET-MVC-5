## 视图
👀视图是用户看到并与之交互的界面。

在Models文件夹中添加一个类(Users)
```
public class Users
   {
       public int Id { get; set; }

       public string Username { get; set; }

       public string Password { get; set; }
   }
}
```
2.在Home控制器里输入内容
```
 public ActionResult Index()
        {
            ViewBag.Title = "第一";
            ViewBag.ProductList = new List<Users>
            {
                new Users
                {
                    Id=1,
                    Username="105度",
                    Password="123456"
                },
                new Users
                {
                     Id=2,
                    Username="完美的他",
                    Password="123456"
                }
            };
           
           
            return View();
        }
```        