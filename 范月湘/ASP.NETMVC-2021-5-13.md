# 好纠结，但不能够优柔寡断
## dynamic
    dynamic的出现让C#具有了弱语言类型的特性.
    dynamic被编译后实际上是一个object类型，只不过编译器会对dynamic类型进行特殊处理，让它在编译期间不进行任何的类型检查，而是将类型检查放到了运行期.

    dynamic类型转换:
    dynamic类型的实例和其它类型的实例间的转换是很简单的，任何实例都能隐式转换为dynamic类型实例:
    dynamic d1=7;
    dynamic d2="a string";
    dynamic d3=System.DateTime.Today;
    dynamic d4=System.Diagnostics.Process.GetProcesses();
    反之亦然，类型为dynamic的任何表达式也能够隐式转换为其它类型.
    int i=d1;
    string str=d2;
    DateTime dt=d3;
    System.Diagnostics.Process[]procs=d4;

    方法中含有dynamic类型参数的重载问题:
    如果调用一个方法是传递了dynamic类型的对象，或者被调用的对象是dynamic类型的，那么重载的判断是发生在运行时而不是编译时.

    dynamic可以简化反射
    dynamic与反射相比，执行相同操作所需的代码少得多,而且性能也提高了一个数量级.
    dynamic比没有优化过的反射性能好，跟优化过的反射性能相当，但代码整洁度高.

![ASP.NET1](./MVC/MVC1.jpg)

![ASP.NET2](./MVC/MVC2.jpg)

![ASP.NET3](./MVC/MVC3.jpg)

## ASP.NET MVC5连接bootstrap
    <link rel="stylesheet" href="bootstrap.min.css"></link>
## 视图传回变量值，如何访问
    asp.net MVC前台View页面向后台Controller控制器传递数据的几种方式
    1.异步方式
    异步方式主要有ajax、post、get三种.
    (1)ajax方式
    /*判断用户是否已经登录*/
        $(function(){
            $.ajax({
                type:"post",
                url:"/MonthWin/Index/12",
                data:{"name":name,"sex":sex},
                dataType:'JSON',
                success:function(result){
                    if(result==true){
                        isLogin=true;
                    }else{
                        isLogin=false;
                    }
                }
            });
        })
        (2)post方式
        $.post(url,{"username":username,"province":province,"city":city,"branch":branch,"number":number,"bankName":bankName},
            function(result){
                if(result=="1"){
                    alert("保存成功");
                    location.reload();
                }else{
                    alert("保存失败");
                    location.reload();
                }
            };
        )
        异步方式实现起来比较简单，可以回传值，根据后台传过来的值进行下一步的操作，同时减少了一次服务器请求，降低了网络负载.

        2.表单方式
        (1)post方式
        <1>不通过js,直接提交的post方式
        <2>使用js提交的post方式
        post传值方式可以传递的数据量几乎不受限制，如果需要大量传值，post方式是个不错的选择.
        (2)get方式
        表单提交的get方式也可以称为QueryString方式，就是简单地把要传递的数据放在Url之后.
        不过get方式相比post的方式有很多不足：传递数据量有限制、不安全

        3.异步表单方式
        异步提交和表单提交的结合使用，结合二者优点，比较方便.

        4.Model数据存储模型方式
        MVC中的Model作为Controller和View之间交流的桥梁，可以在View和Controller之间传递数据.
        实现主要分为以下三步:
        (1)定义Model实体
        (2)定义Html标签
        (3)传值
        在数据存储模型传值方式中，也采用了ajax异步方式，然而与ajax又有一些不同:传递的数据是Model实体类型.

        5.Url路由方式
        MVC开发方式中，请求的是控制器下面的一个Action,有时Action需要参数，就是路由参数.通过添加路由规则传递路由参数.
        Url路由方式传值方式的数据量很有限，不适于大数据量的传递.