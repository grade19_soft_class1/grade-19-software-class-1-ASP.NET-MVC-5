# 摸鱼笔记
    1.在Models建BaseModel、Comments、Messages、Users类,后三者继承BaseModel

    2.在Controllers控制器添加NuGet程序包

    3.在Web.config中修改服务器(server)、数据库名(database)、用户名(uid)及密码(pwd)

    4.于***Db添加public System.Data.Entity.DbSet<SayDemo.Models.Messages>Messages{get;set;}

    5.于Messages中的index.cshtml文件添加之前写好的html，加上Layout=null;

    6.link标签引入bootstrap.min.css等其他需要引入的文件，可以更新版本

    7.创建一个js文件并引入，从之前建的js文件加入代码

    8.于RouteConfig.cs文件修改控制器名称

    9.于项目创建一个新的文件夹并在其中添加一个类，使新文件夹与其他文件夹同等级

    10.可以于index.cshtml中更换模块名，添加using 项目名.Models;

    11.遍历吐槽模块并修改引用名称

    12.于新建文件夹中的类添加对象

    {
        Username:'3447',
        Messages:[{
            FromUserId:1,
            Content:[{
                FromUserName:'来自谁的评论',
                ToUserName:'',
                Comment:''
            }]
        }]
    }
    要整明白这其中的关系

    13.新建另一个文件夹添加类并遍历

    14.于控制器中新建的类修改对象名，运行实验

