## 第三节 我们可以稍微插入几个数据

## 结构如下
![图片错误](./img/57.jpg)

## 这里是Db中的 SayDemoDb.cs 这边主要引入了三个表 用于连接到数据库 
```
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SayDemo.Data
{
    public class SayDemoDb : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public SayDemoDb() : base("name=SayDemoDb")
        {
        }

        public System.Data.Entity.DbSet<SayDemo.Models.Messages> Messages { get; set; }

        public System.Data.Entity.DbSet<SayDemo.Models.Comments> Comments { get; set; }

        public System.Data.Entity.DbSet<SayDemo.Models.Users> Users { get; set; }
    }
}
```




## 这里是Db 中的 SayDamoDblnitilizer.cs

```
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using SayDemo.Models;

namespace SayDemo.Data
{
    public class SayDamoDblnitilizer:DropCreateDatabaseAlways<SayDemoDb>
    {
        protected override void Seed(SayDemoDb db)
        {
            db.Users.Add(new Models.Users
            {
                Username = "admin",
                Password = "123",
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                Version = 0
            });

            db.Users.Add(new Models.Users
            {
                Username = "h",
                Password = "111",
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                Version = 0
            });

            db.Users.Add(new Models.Users
            {
                Username = "310",
                Password = "310",
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                Version = 0
            });



            db.Messages.AddRange(new List<Messages>
            {
                new Messages
                {
                    FromUserId=1,
                    Content="真是搞不懂你诶",
                    CreatedAt=DateTime.Now,
                    UpdatedAt=DateTime.Now,
                    Version=0
                },
                new Messages
                {
                    FromUserId=2,
                    Content="我这现在有一套很大的房子你要不咯",
                    CreatedAt=DateTime.Now,
                    UpdatedAt=DateTime.Now,
                    Version=0
                }
            });


            db.Comments.AddRange(new List<Comments>
            {
                new Comments
                {
                    FromUserId=1,
                    MsgId=1,
                    Content="这个难道还要我回复你？？？",
                    CreatedAt=DateTime.Now,
                    UpdatedAt=DateTime.Now,
                    Version=0
                },

                new Comments
                {
                    FromUserId=2,
                    MsgId=2,
                    Content="好了这又是一个",
                    CreatedAt=DateTime.Now,
                    UpdatedAt=DateTime.Now,
                    Version=0
                }
            });

            db.SaveChanges();

            base.Seed(db);
        }

    }
}
```