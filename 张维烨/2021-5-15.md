## 一、理解ViewBag、ViewData
```
    Ⅰ、ViewData与ViewBag的区别

    1、ViewData是字典类型，赋值方式用字典方式，通过key值读取对应的value，ViewData[“myName”]

    2、ViewBag是动态类型，使用时直接添加属性赋值即可ViewBag.myName

    3、ViewBag和ViewData只在当前Action中有效，等同于View

    4、ViewData和ViewBag中的值可以互相访问，因为ViewBag的实现中包含了ViewData

    Ⅱ、ViewBag 通俗翻译应该是为可视包

```

## 二、创建表模型，传入值到网页界面
```
    1、首先创建类(.cs)，设置想设置的值或者属性，这里以Users.cs为例
    2、在想要显示界面的控制器文件中引入Users.cs所在的父级文件夹
    3、使用ViewBag定义所要显示的值
    4、在所要显示的cshtml界面引入想要显示的值
```