# 课堂笔记

## 创建数据库表的结构原则
* 表名加前缀
* 所有的字段选择最合适的最小的数据类型，如Id使用mediumint比int节省25%的空间，表越小，查询的速度就越快
* 尽量将所有的字段都设置为not null，这样能让速度更快
* 为合适的字段添加索引

### 事务ACID属性
原子性atomicity，一致性consistency、隔离性Isolation，持久性Durability

8 原子性，事务的原子性表示事务执行过程中，把事务作为一个工作单元处理，一个工作单元可能包括若干个操作步骤，每个操作步骤都必须完成才算完成，若因任何原因导致其中的一个步骤操作失败，则所有步骤操作失败，前面的步骤必须回滚。
*一致性，事务的一致性保证数据处于一致状态。如果事务开始时系统处于一致状态，则事务结束时系统也应处于一致状态，不管事务成功还是失败。
* 隔离性，事务的隔离性保证事务访问的任何数据不会受到其他事务所做的任何改变的影响，直到该事务完成。
* 持久性，事务的持久性保证加入事务执行成功，则它在系统中产生的结果应该是持久的。

## 面向对象的特点
* 封装
* 继承
* 多态
```
//父类
public class Base {
	protected void show() {}
}

//子类
class Kid extends Base {
	 public  void show() {
		System.out.println(" i am  kid");
	}
}
```
```
public static void main( String[] args )
    {
    	Kid kid = new Kid();
    	Base base = kid;
    	base.show();
    }
```