# 课堂笔记
## 注册登录的逻辑
```
1.首先创建一个控制器，模型类选择Users，上下文类用创建Messages时那个上次那个即可
2.我们在Users下创建两个视图，命名为Register和Login
3.创建一个新的文件夹，命名为ParamModel里面创建两个类分别命名为：RegisterModel和LoginModel，
```

### 前往Users控制器首先创建GET请求命名为Register和Login，再创建两个POST请求分别命名为RegisterDone和LoginDone
```
[HttpPost]
        public ActionResult RegisterDone(RegisterModel registerModel)
        {
            var username = registerModel.Username.Trim();
            var password = registerModel.Password.Trim();
            var repetitionPassword = registerModel.RepetitionPassword.Trim();

           
            if (username.Length > 0 && password.Length > 0 && password.Equals(repetitionPassword))
            {
                var user = db.Users.Where(x => x.Username.Equals(username)).SingleOrDefault();
                if(user != null)
                {
                    return Content("<script>alert('用户名已存在');location.href='Register';</script>");
                }
                else
                {
                    db.Users.Add(new Users
                    {
                        Username = username,
                        Password = password,
                        CreateAt = DateTime.Now,
                        UpdateAt = DateTime.Now,
                        Version = 0
                    });

                    db.SaveChanges();
                    return Content("<script>alert('注册成功');location.href='Login';</script>");
                }
            }
            else
            {
                return Content("<script>alert('用户名或密码不能为空，并且两次密码应一致');location.href='Register';</script>");
            }
        }
```

```
[HttpPost]
        public ActionResult LoginDone(LoginModel loginModel)
        {

            var username = loginModel.Username.Trim();
            var password = loginModel.Password.Trim();
            var userList = db.Users.ToList();
            if (username.Length > 0 && password.Length > 0)
            {


                var user = db.Users.Where(x=>x.Username.Equals(username) && x.Password.Equals(password)).SingleOrDefault();
                if (user != null)
                {
                    return Content("<script>location.href='/';</script>");
                }
                else
                {
                    return Content("<script>alert('用户名或密码不正确');location.href='Login';</script>");
                }
            }
            else
            {
                return Content("<script>alert('用户名或密码不能为空');location.href='Login';</script>");
            }
        }
```