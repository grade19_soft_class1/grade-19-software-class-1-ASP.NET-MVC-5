# 模型！！！
```
数据模型，提供要展示的数据，因此包含数据和行为，可以认为是领域模型或JavaBean组件（包含数据和行为），不过现在一般都分离开来：Value Object（数据） 和 服务层（行为）。也就是模型提供了模型数据查询和模型数据的状态更新等功能，包括数据和业务。
```
## 在model下创建一个cs（随便）
```
  public class Users
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
```
## 修改文件 让数据库可以连接
### web.config 底下的文件
```
如果是mysql 就改成
    <add name="SayDemoDb" connectionString="server=.;database=SayDemo;uid=sa;pwd=123456;" providerName="System.Data.SqlClient" />

如果是
   连接postgresql 
   将 providerName="System.Data.SqlClient" 更改为 Npgsql

```
![图片](./imgs/2.jpg)

## 为了可以正常访问，还需要将APP_Start地下的Rourtconfig.CS修改成你需要访问的视图文件名┗|｀O′|┛ 嗷~~
![图片](./imgs/1.jpg)