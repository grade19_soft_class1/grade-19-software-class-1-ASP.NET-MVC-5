# mvc 的书网上版！
```
链接：https://pan.baidu.com/s/1o7WaazYwihrgYstxak3olQ 密码：vc78
```
# mvc 的微软教程
```
https://docs.microsoft.com/zh-cn/aspnet/core/mvc/views/overview?view=aspnetcore-5.0
```
# ASP.NET MVC 5!!!!!
```
这是一种构建Web应用程序的框架！
```
### MVC是什么呢？
```
将应用程序的用户界面 分为三个主要部分
1.模型：一组类，描述了要处理的数据及修改操作的业务规则。
2.视图：定义应用程序用户界面的显示方式。
3.控制器：一组类，用于处理来自用户.整个应用程序流以及特定应用程序逻辑的通信！
```
### 当我们创建了一个项目的时候 右边的文件夹代表什么呢？
```
1.App_Data 文件夹
 用于存储应用程序数据。
 ```
 ```
2.Content 文件夹
Content 文件夹用于静态文件，比如样式表（CSS 文件）、图表和图像。
```
```
3.Controllers 文件夹
Controllers 文件夹包含负责处理用户输入和响应的控制器类。
```
```
4.Models 文件夹
Models 文件夹包含表示应用程序模型的类。模型存有并操作应用程序的数据。
```
```
5.Views 文件夹
Views 文件夹存有与应用程序的显示相关的 HTML 文件（用户界面）。

Views 文件夹中含有每个控制器对于的一个文件夹。
```
## 我们来总结一下吧！！
```
1.在控制器里面，查找返回视图的时候，默认查找的是和控制器同名的View目录下的视图文件，这很重要，所以你尽量要同名！例如：Views/Product/Index.cshtml,如果你不同名的话，也可以手动指定返回不同的视图哦
a.如果你是同名！可以简写为类似Details的形式
b.如果你不同名，则需要指定完整的视图路径 比如：~/Views/Home/Index.cshtml

2.默认有定义一个简单的路由，例如:controller/action/id

3.传参有两种方法（get方式）
a. 在url中使用?name =xxxxxx的方法，然后在控制器中以同名的变量去获得传过来的值 例如：
```
1.
![图片](./imgs/2021-05-11_172040.jpg)
2.
![图片](./imgs/2021-05-11_172055.jpg)
```
b.在url中使用类似：product/details/xxxxx的形式，将xxx传入控制器,控制器以id或其他自行定义的变量来获取传来的参数
```