## 关联数据库
```
定义一个基本模型给 Users，Messages，Comments 几个类继承
BaseModel
public int Id{get;set;}
public Date CreateAt{get;set;}
public Date UpdateAt{get;set;}
public int Version{get;set;}
public string Remarks{get;set;}

```
```
添加控制器
1、选择包含视图的MVC5控制器
2、选择要生成的哪个模型的控制器
3、在web.config文件里修改连接数据库的字段
4、在Global.asax.cs中添加  
Database.SetInitializer(new DropCreateDatabaseAlways<WebApplication1.Data.WebApplication1Db>()); 
//这行代码是为了将数据库重置
5、我们在Data的文件夹下添加一个 
public System.Data.Entity.DbSet<WebApplication1.Models.User> Users { get; set; }
// WebApplication1.Models.User 是 文件路径 Users是表名
```
```
使用postgresql数据库
1、打开管理NuGet程序
2、修改web.config中
<add name="WebApplication1Db" connectionString="server=172.16.10.214;database=artion;uid=postgres;pwd=qq_112358" providerName="Npgsql" />
3、修改Data文件夹中的代码
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //EF 默认的schema 是dbo，但是PG默认是public，这里改一下
            modelBuilder.HasDefaultSchema("public");
        }
4、在数据库刚刚建成时就插入数据
            var JianLai = new Users
            {
                Username = "陈平安",
                Password = "123"
            };
            db.Users.Add(JianLai);
            db.SaveChanges();
```