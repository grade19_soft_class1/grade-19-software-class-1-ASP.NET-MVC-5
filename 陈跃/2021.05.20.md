## 完成登陆注册逻辑
### 现在视图那引入之前说说的登陆注册模板
### 建一个ParamModel文件夹用来传用户名密码对象
![](./img/param.png)
![](./img/login.png)
![](./img/reg.png)
### 建一个关于Users的控制器(添加以下代码)
```
 public ActionResult Login()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public string registerDone(RegisterModel registerModel)
        {
            var username = registerModel.Username.Trim();
            var password = registerModel.Password.Trim();
            var confirmPassword = registerModel.ConfirmPassword.Trim();

            dynamic result;

            // 判断用户名、密码是否为空，不为空则继续；否则直接返回信息
            if (username.Length > 0 && password.Trim().Length > 0 && password.Equals(confirmPassword))
            {
                 var user = db.Users.Where(x => x.Username.Equals(username)).FirstOrDefault();

                // 如果用户不为空，则表示该用户名已经注册
                if (user != null)
                {
                    result = new
                    {
                        code = 1000,
                        msg = "当前用户名已经存在"

                        
                    };
                }
                else
                {
                    db.Users.Add(new Users
                    {
                        Username = username,
                        Password = password,
                        CreatedAt=DateTime.Now,
                        UpdatedAt=DateTime.Now,
                        Version=1,
                    });

                    db.SaveChanges();
                    result = new
                    {
                        code = 200,
                        msg = "注册成功"
                    };
                }
            }
            else
            {
                result = new
                {
                    code = 1000,
                    msg = "用户名密码不能为空，并且两次密码应该一致，请核对后重试=_="
                };
            }
            return JsonConvert.SerializeObject(result);
        }
        
        public string logindone(LoginModel login)
        {
            dynamic result;
            if (login.Username.Trim().Length > 0 && login.Password.Trim().Length > 0)
            {
                var user = db.Users.Where(x => x.Username.Trim().Equals(login.Username.Trim())
                  && x.Password.Trim().Equals(login.Password.Trim())).FirstOrDefault();

                if (user != null)
                {
                    result = new
                    {
                        code = 200,
                        msg = "登陆成功"
                    };
                }
                else
                {
                    result = new
                    {
                        code = 1000,
                        msg = "用户名或密码错误"
                    };
                }
            }
            else
            {
                result = new
                {
                    code = 1000,
                    msg = "用户名或密码不能为空,请输入用户名和密码"
                };
            }

            return JsonConvert.SerializeObject(result);
        }
```

### 把login视图的js文件中的url改成
![](./img/ld.png)

### 把register视图的js文件的url改成
![](./img/rg.png)