# asp.net 第六课


```

protected void app_start(){
   /*注册区域*/
   /*绑定路由*/
   Database.SetInitializer(new DB<DB>());  //或者实列化数据库类,DB继承了CreateDatabaseIfNotExist类
   using(var DB context=new DB()){
      DB.Database.Initialize(true);//数据库初始化操作
   }

```
## 在Global.asax中添加 Database.SetInitializer(new DB<DB>());
DB是定义的模版，如下：
```
 public class sayDbinit:DropCreateDatabaseAlways<MyPostgresDB>
    {
        protected override void Seed(MyPostgresDB dB)
        {
            dB.UserInfoes.Add(new Models.UserInfo
            {
                UserName = "admin",
                Password = "123",
                CreateTime=DateTime.Now,
                UpdateTime=DateTime.Now,
                IsActive=true
            });


            dB.Messages.AddRange(new List<Message> 
            {
              new Message
              {
                  UserId=1,
                  Content="山无棱天地合，江水为竭；冬雷震震，夏雨雪，乃敢与君绝",
                  CreateTime=DateTime.Now,
                  UpdateTime=DateTime.Now,
                  IsActive=true
              }
            });

            dB.Comments.AddRange(new List<Comment>
            {
                new Comment
                {
                    fromUserId=1,
                    toUserId=1,
                    MessageId=1,
                    Content="山有木兮木有枝，心悦君兮君不知",
                    CreateTime=DateTime.Now,
                    UpdateTime=DateTime.Now,
                    IsActive=true
                }
            });


            dB.SaveChanges();



            base.Seed(dB);
        }
    }
```
其中db.SaveChanGes();是用于在数据库更改已经定义的数据的