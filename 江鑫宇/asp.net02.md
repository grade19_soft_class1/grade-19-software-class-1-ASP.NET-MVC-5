# asp.net 第二课
```
namespace WebApplication4.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
```
1. 如上代码就是创建asp.net mvc5 时HomeController控制器的代码这里包含了三个不同的方法，这些方法在访问到对应路由时触发。例如访问localhost：port/home/about就是触发About方法   
2. 以上方法返回的view()默认返回的是/Views目录下和方法同名的视图。例如：About默认返回的就是views下的名称为About的视图
3. ViewBag可以用于向视图传递数据，例如ViewBag。Message="Hello。Word！"在视图@ViewBag.Message的值就是"Hello.Word!"