# asp.net 第四课
1.向视图传值前做的准备
```
 在新建项目中的Models文件下新建类,这里以Products为例：

public class Products
 {
   public int Id { get; set; }
   public string Name { get; set; }
   public double Price { get; set; }
 }


2. 在控制器中实例化此类
var p = new Products()
      {
        Id = 1,
        Name = "饮料",
        Price = 2.5
      };

```
## 方式1：ViewData
将控制器中的方法用ViewData以键值对的形式类存储上述的实例化对象，如下:
```
ViewData["person"] = p;
然后在视图中获取ViewData中的值，并转换对象，如下：

@{
  var p = (Products)ViewData["person"];
}
<h1>@p.Id</h1>
<h2>@p.Name</h2>
<h3>@p.Price</h3>
```
## 方式2：ViewBag
将控制器中的方法利用ViewBag动态表达式的形式存储上述对象，如下：

```
ViewBag._Product = p;

@{
  var p = (Products)ViewBag._Product;
}
```
## 方式3：Model

将控制器中的方法返回View上述对象，如下：
```

public ActionResult Index()

    {

      var p = new Products()

      {

        Id = 1,

        Name = "饮料",

        Price = 2.5

      };

      return View(p);

    }
而我们在视图中得到强制类型对象Products，如下：


@using MvcTest.Models;
@model Products
@{
  ViewBag.Title = "Index";
}
<h1>@Model.Id</h1>
<h2>@Model.Name</h2>
<h3>@Model.Price</h3>

```
## 方式四：TempData

TempData可以通过转向继续使用，因为它的值保存在Session中。但TempData只能经过一次传递，之后会被系统自动清除。
```


下面我将演示从Index动作转向Order动作，并在视图中输出TempData中存储的值。

首先在控制中新建Action方法，命名为Order方法，代码如下：

public ActionResult Index()
    {
      var p = new Products()
      {
        Id = 1,
        Name = "饮料",
        Price = 2.5
      };
      TempData["_product"] = p;
      return RedirectToAction("Order");
    }
    public ActionResult Order()
    {
　　　　　　return View();
    }

```
## 修改视图如下：
```


@{
  Products p = (Products)TempData["_product"];
}
假设控制器中的代码修改如下：

public ActionResult Index()
    {
      var p = new Products()
      {
        Id = 1,
        Name = "饮料",
        Price = 2.5
      };
      TempData["_product"] = p;
      return RedirectToAction("Order");
    }
    public ActionResult Order()
    {
      return RedirectToAction("Detail");
    }
    public ActionResult Detail()
    {
      Products _product = (Products)TempData["_product"];
      return View("");
    }

```
转向：Index — Order — Detail，那么在Detail方法中，是不能获取到TempData对象的，因为TempData只能经过一次传递，之后会被系统自动清除。