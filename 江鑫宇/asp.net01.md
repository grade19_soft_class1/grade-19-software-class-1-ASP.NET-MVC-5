# asp.net mvc 第一课
1. asp.net mvc 使用的是视图模型和控制器到的开发模式
2. asp.net mvc的程序目录如下   
![](./img/程序结构.png)   
/Controllers  该目录用于保存处理URL请求的Controller类   
/Models 该目录用于保存那些表示和操作数据的类   
/Views 该目录用于保存负责呈现输出结果（例如：html）的UI模版文件   
/Scripts 该目录用于保存Javascript库文件和js脚本   
/Content 用于存放CSS文件，图片和其他站点内容。非脚本   
/App_Data 用于存放想要读取和写入数据的文件   
/App_Start 用于保存一些功能代码，例如路由