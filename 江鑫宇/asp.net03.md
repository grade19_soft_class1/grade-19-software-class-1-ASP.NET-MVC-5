# asp.net 第三课
## 视图
```
 @{
 2     Layout = null;
 3 }
 4 
 5 <!DOCTYPE html>
 6 
 7 <html>
 8 <head>
 9     <meta name="viewport" content="width=device-width" />
10     <title>UserRegist</title>
11 </head>
12 <body>
13     <div> 
14         <form action="/UserInfo/ProcessUserRegist" method="post">
15             <table>
16                 <tr>
17                     <td>用户名:</td><td><input type="text" name="UserId" /></td>
18                 </tr>
19                 <tr>
20                     <td>密码：</td><td><input type="password" name="pwd" /></td>
21                 </tr>
22                 <tr>
23                     <td colspan="2">
24                         <input type="submit" value="提交"/>
25                         <input type="reset" value="重置"/>
26                     </td>
27                 </tr>
28             </table>
29         </form>
30     </div>
31 </body>
32 </html>
```
## 方式一
```


          public ActionResult ProcessUserRegist()
          {
              string userId = Request.QueryString["UserId"] ?? Request.Form["UserId"];
              string pwd = Request.QueryString["pwd"] ?? Request.Form["pwd"];
              return Content(userId + "注册成功!"); //相当于 Response.Write("");
              Response.End();
         }
```
## 方式二
```

         public ActionResult ProcessUserRegisret(FormCollection collection)
         {
             string userId = collection["UserId"];
             return Content(userId + "注册成功!"); //相当于 Response.Write("");
             Response.End();
         }
```
## 方式三
```

         public ActionResult ProcessUserRegist(string UserId,string pwd)
         {
             return Content(UserId + "注册成功!"); //相当于 Response.Write("");
             Response.End();
        }
```
## 方式四
```

         public ActionResult ProcessUserRegist(Result result)
         {
             return Content(result.UserId + "注册成功!"); //相当于 Response.Write("");
             Response.End();
         }
         public class Result
         {
             public string UserId { get; set; }
             public string pwd { get; set; }
         }
```