# 视图
```
视图与控制器有关，
在控制器中可以显示视图中的内容，在视图中也可以引用控制器中的参数或者值

在控制器中可以通过ViewBag.名称的方式在视图中显示ViewBag中的内容
```
### 在视图中引入bootstrap
```
 1. @Styles.Render("~/Content/css");
 2. <link rel="stylesheet" href="~/Content/booststrap.min.css"/>
```
### 强类型视图
```
@using WebApplication1.Models;
```
### 在models中创建类
```
namespace WebApplication1.Models
{
    public class Product
    {
        public int Id { get; set; }

        public string ProductName { get; set; }
    }
}
```
### 视图中的标题
```
ViewBag.Title = "小毛驴";
```
### 控制器传入视图
```
public ActionResult Index()
        {

            ViewBag.Title = "小毛驴";
            ViewBag.Project = new List<Users> //<>类
            {
            new Users // <>类
            {
                Id = 2,
                username = "我有一头小毛驴",
                text = "从来也不骑有一天心血来潮骑着它去赶集"
            }
            };
            return View();
        }
```
### 视图
```
@{
    ViewBag.Title = "index";
}

<html>
<head>
    <title>@ViewBag.Title</title>
</head>
    @Styles.Render("~/Content/css");
    <link rel="styl" href="~/Content/bootstrap.min.css"/>
    <style>
        li{
            font-family:Arial;
            font-size:90px;
        }
    </style>

<body>
        我有一头小毛驴从来也不骑    
</body>
</html>
```
