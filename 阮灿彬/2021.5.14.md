# 模型
### 创建模型类 Models-users
```
namespace WebApplication1.Models
{
    public class Users
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string password { get; set; }
    }
}
```
添加已搭建基架的新项 模型类Users (WebApplication1.Models)
## 连接数据库增删改查 
```
    
    在Data文件夹里添加一段代码
    原因ef默认的schema是dbo，但是pg默认是public
    protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");
        }
```
### 更改Web.config 里的数据库连接
```

   如是连接postgresql 
   将 providerName="System.Data.SqlClient" 更改为 Npgsql
```
![](./imgs/2021-05-16_1.png)
### 如果是 pgl 则要添加程序包
！[](./imgs/2021-05-16_2.png)
![](./imgs/2021-05-16_3.png)

### 跳转
![](./imgs/2021-05-16_4.png)
