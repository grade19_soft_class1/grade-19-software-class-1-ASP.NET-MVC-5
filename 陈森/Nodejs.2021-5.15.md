### Mvc连接数据库

#### 使用sql server 数据库
```
新建一个Users类,
 public class Users
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public int Password { get; set; }
    }

然后到控制器添加一个包含视图的Mvc5 控制器，添加模型类为Users，添加数据上下文类（请先重新生成一下解决方案，否则可能报错）

然后到解决方案底下的Web.config修改一下数据库的默认连接：//数据库可自动生成不用手动创建
 <connectionStrings>
    <add name="MyappDb" connectionString="server=.;database=Chendada;uid=sa;pwd=123456;" providerName="System.Data.SqlClient" />
  </connectionStrings>

设置好就可以愉快的增删改查了
```
![](./imgs/111.png)

![](./imgs/1111.png)
```
在解决方案底下的Global.asax添加:

 Database.SetInitializer(new DropCreateDatabaseAlways<Myapp.Data.MyappDb>());//让它总是删除新创建的数据库
```
```
在删除数据库的时候会报数据库正在使用的错误，需要先断开连接/手动删库 

```
![](./imgs/00.png)
### postgres
```
在引用模块点击管理Nuget程序包，搜索ef postgres下载 （不是core的跨平台版本），然后重启一下软件
在web.config把连接配置改为：
 <add name="WebApplication1Dd" connectionString="server=120.25.27.226;database=Chendada;uid=postgres;pwd=cnm200136zbc.; " providerName="Npgsql" />


 由于数据库生成的表前缀不同，到数据上下文添加：
  protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //EF 默认的schema 是dbo，但是PG默认是public，这里改一下
            modelBuilder.HasDefaultSchema("public");
        }

打开xshell查询数据库,切换到Chendada数据库
\db 查看所有的表   查看大写开头的表加双引号


```
![](./imgs/3333.PNG)




