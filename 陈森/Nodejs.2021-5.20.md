### 螺旋也听不懂😢
#### 登陆和注册
```
新键一个带视图的控制器Users模型，然后views添加登陆和注册的视图
新建一个文件夹包装LoginModel，RegisterModel的模型类
 
RegisterModel：
public class RegisterModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }

LoginModel：
public class LoginModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

```
#### 控制器
```
 public ActionResult Register()
        {


            return View();
        }

public ActionResult Login(LoginModel login)
        {                     

            return View();
        }

 //添加方法返回视图       


```
#### 控制器后端处理
```
  [HttpPost]
        public string registerDone(RegisterModel registerModel)
        {
            var username = registerModel.Username.Trim();
            var password = registerModel.Password.Trim();
            var confirmPassword = registerModel.ConfirmPassword.Trim();

            dynamic result;

            // 判断用户名、密码是否为空，不为空则继续；否则直接返回信息
            if(username.Length>0 && password.Trim().Length>0 && password.Equals(confirmPassword))
            {
                var user = db.Users.Where(x => x.Username.Equals(username)).FirstOrDefault();

                // 如果用户不为空，则表示该用户名已经注册
                if (user != null)
                {
                    result = new
                    {
                        code = 1000,
                        msg = "当前用户名已经存在"
                    };
                }
                else
                {
                    db.Users.Add(new Users
                    {
                        Username = username,
                        Password = password
                    });

                    db.SaveChanges();
                result = new
                    {
                        code = 200,
                        msg = "注册成功"
                    };
                }


            }
            else
            {
                result = new 
                { 
                    code=1000,
                    msg="用户名密码不能为空，并且两次密码应该一致，请核对后重试=_="
                };
            }


            return JsonConvert.SerializeObject(result);
        }


     public string LoginDone (LoginModel login)
        {
            dynamic result;

            if (login.Username.Trim().Length > 0 && login.Password.Trim().Length > 0)
            {
                var user = db.Users.Where(x => x.Username.Equals(login.Username.Trim()) && x.Password.Equals(login.Password.Trim())).FirstOrDefault();

                if (user != null)
                {
                    result = new
                    {
                        code = 200,
                        msg = "登录成功"
                    };
                }
                else
                {
                    result = new
                    {
                        code = 1000,
                        msg = "用户名或密码不正确，请核对后重试"
                    };
                }
            }
            else
            {
                result = new
                {
                    code = 1000,
                    msg = "用户名密码不能为空，请核对后重试=_="
                };
            }

            return JsonConvert.SerializeObject(result);
        }



```
```
报错datatime问题：原因是BaseModel模型里缺少数据默认定义：
 public BaseModel()
        {
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
            Version = 0;
        }
```