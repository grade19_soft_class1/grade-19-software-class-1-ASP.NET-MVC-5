### 今天下雨😀
#### 定义模型类
```
在git创建一个说说的仓库，用来提交进程代码
添加忽略文件，选择vs studio 模板


创建所需要的以下类：
BaseModel.cs用来当继承类：
  public abstract class BaseModel
        {
            public int Id { get; set; }
            public DateTime CreatedAt { get; set; }
            public DateTime UpdatedAt { get; set; }
            public int Version { get; set; }
            public string Remarks { get; set; }
        }


Messages.cs模型类:
 public class Messages : BaseModel
        {
            public int FromUserId { get; set; }
            public string Content { get; set; }
        
        }

Comments.cs:
 public class Comments : BaseModel
    {
        public int MsgId { get; set; }
        public int FromUserId { get; set; }
        public string Content { get; set; }

    }
 Users.cs:
 public class Users:BaseModel
    {
       public string Username { get; set; }
       public int Password { get; set; }

    }   


```
### 修改连接配置环境
```
创建一个Messages数据库模型类：

在web.config把数据库默认连接改为server的连接：
<add name="AppsensaysayDb" connectionString="server=.;database=Chendada;uid=sa;pwd=123456;"providerName="System.Data.SqlClient" />

在数据库上下文添加创建数据表：
public System.Data.Entity.DbSet<Appsensaysay.Models.Messages> Messages { get; set; }
public System.Data.Entity.DbSet<Appsensaysay.Models.Users>Users { get; set; }
public System.Data.Entity.DbSet<Appsensaysay.Models.Comments> Comments { get; set; }


在引用功能把管理NuGet包把所需包更新：Jquery，Bootstrsp
 
```

#### index主页
```
把我们的做好微说说的页面放进去：
修改一下css，js和imgs的引用
然后把Messages/index.cshtml修改为：
@Layout = null; 不引用vs系统生成的网页格式


添加一个ViewsModel文件夹：
ViewModelMessage.cs:
pubilc string Username {get;set;}
pubilc IEnumerable <Messages>Messages{get;set;}//定义接口，用来遍历

index.cshtml:
@model Appsensaysay.ViewModel.ViewModelMessage//引用
吐槽模块：
遍历元素： @foreach (var msg in Model.Messages)
{
    @msg.Content
    @msg.Id


}
                    
当前用户：@Model.Username 

MessagesController.cs控制器输出：
public ActionResult Index()
        {
            var data = new ViewModelMessages
            {
                Username = "变傻子",
                Messages=new List<Message>
                {
                    new Messages{
                        Id=1,
                        FromUserId=233,
                        Content="我的第一条说说"
                    }
                }
        return View(data);//还没有进行数据库连接查询，返回data数据






```
#### 数据逻辑框架为下图
![](./imgs/000.PNG)