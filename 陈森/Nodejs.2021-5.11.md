#### 今天学习了一个新课 Mvc
![](./imgs/33.png)

使用vs studio 2019 进行学习


## 新建一个项目后是下面这样
![](./imgs/11.png)

然后在Controllers控制器添加一个控制器
在Views下生成一个文件夹，添加视图


![](./imgs/55.png)

### 在Controllers控制器下cs文件中设置加载的方法
![](./imgs/方法.png)
传进绝对路径（默认指向index）,返回指定路径下的视图

### 传参（get）
```
在URL中使用name?=xxx 形如 /Hcahtml/Eruu?name=xxx
在URL中使用类似： /Hcahtml/ID?id=xxx，将值为8的参数传递到控制器，控制器以id或者其它自行定义的变量接收

```

