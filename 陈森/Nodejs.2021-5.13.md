### 视图
### Dynamic
```
dynamic是FrameWork4.0的新特性。dynamic的出现让C#具有了弱语言类型的特性。编译器在编译的时候不再对类型进行检查，编译期默认dynamic对象支持你想要的任何特性。
dynamic关键字可充当C#类型系统中的静态类型声明。这样，C#就获得了动态功能，同时仍然作为静态类型化语言而存在.


```
### ViewBag
```
controller向view传值用的,ViewBag是动态类型，使用时直接添加属性赋值即可

 public ActionResult Index()
        {

            List<User> users = new List<User> //<User>传进的是一个类
            {
                new User
                {
                    Id=111,
                    Username="牛啊牛啊",
                    Password=113
                }

            };
            ViewBag.list =users;

            return View();
        }
       

       <ul>
        
         @foreach (var item in ViewBag.list)
        {
        <li>ID: @item.Id</li>

        <li> 用户名： @item.Username </li>

        <li>密码：@item.Password</li> 
        }
        </ul>
    

```
![](./imgs/44.png)
