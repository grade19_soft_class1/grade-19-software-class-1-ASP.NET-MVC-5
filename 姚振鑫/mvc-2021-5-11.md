# Mvc第一次课堂笔记

# 1. 控制器 

# 2. 控制器中，返回试图的时候，默认查找的是和控制器同名（忽略controller）的View下的目录的试图文件，形如 Views/Prodect/Index.cshtml同时，也可以手动指定返回不同的试图
a.如果是在控制器同名的试图目录，则可以简写为类似Details形式
b.反之，需要指定完整的试图的路径，形如"~/Views/Home/Index.cshtml"

# 3 默认有定义一个简单的路由如：controller/action/id

# 4传参的俩种形式（get方式）

a.在url中使用?bane=xxxx的方式，然后在控制器中以同名的变量去获得传过来的值
b.在url中使用类似:product/details/8的形式，将值为8的参数传递到控制器，控制器以自定义的变量获得传过来的值.