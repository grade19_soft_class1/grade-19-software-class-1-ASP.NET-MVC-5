## 课堂笔记
### 微说说的登录注册以及跳转
#### 我们先在UsersController.cs敲上一点代码
```
        public ActionResult Register()
        {
            return View();
        }

        public ActionResult Login(LoginModel login)
        {
            return View();
        }
```
#### 然后我们以上代码为基础右键点Register()和Login()创建相对的视图
#### 之后我们在Register.cshtml中做注册的
```
@{
    Layout = null;
}
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>注册</title>
    <link rel="stylesheet" href="~/Content/bootstrap.min.css">
    <link rel="stylesheet" href="~/Content/register.css">
</head>

<body>
    <section>
        <video id="v1" autoplay loop muted>
            <source src="~/imgs/kda.mp4" />
        </video>
        <div class="box">
            <div class="box">
                <div class="alert alert-danger alert-dismissible fade show invisible text-center" style="width: 25rem; margin-top: -8rem;"
                     role="alert">
                    <strong>提示：</strong><label for="" id="msg" style="height: 0.5rem; width: 9rem;">555</label>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="container">
                    <div class="form">
                        <h2>微说说</h2>
                        <form id="registerForm" action="/registerDone" method="POST">
                            <div class="inputBox">
                                <input type="text" id="username" name="username" placeholder="账号">

                            </div>
                            <div class="inputBox">
                                <input type="password" id="password" name="password" placeholder="密码">
                            </div>
                            <div class="inputBox">
                                <input type="password" id="confirmpassword" name="confirmpassword" placeholder="确定密码">
                            </div>
                            <div class="inputBox">
                                <button type="button" class="btn btn-outline-primary" onclick="jump()">注册</button>
                                <button type="button" class="btn btn-outline-primary" onclick="cancle()">取消</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    </section>
    <script src="~/Scripts/jquery-3.6.0.min.js"></script>
    <script src="~/Scripts/bootstrap.min.js"></script>
    <script>
        function jump() {
            let username = $('#username').val();
            let password = $('#password').val();
            let confirmpassword = $('#confirmpassword').val();
            if (username.length > 0 && password.length > 0 && password === confirmpassword) {
                $.ajax({
                    url: "/users/registerDone",
                    type: "POST",
                    data: $('#registerForm').serialize(),
                    dataType: "json",
                    success: function (res) {
                        console.log(res);
                        if (res.code === 200) {
                            window.location.href = "/users/login";
                        } else {
                            console.log(res.msg);
                        }
                    },
                    error: function (msg) {
                        console.log('网络或者服务器有问题');
                    }
                })
            } else {
                alert_fn();
                return false;
            }
        }
        function alert_fn(msg) {
            msg = msg || "密码不能为空，密码和确定密码必须一致";
            $('#msg').text(msg);
            $('.invisible').removeClass('invisible');
            setTimeout(() => {
                $('.alert').addClass('invisible')
            }, 3000);
        }
        function cancle() {
            window.location.href = '/users/login'
        }
    </script>
</body>
</html>
```
#### 之后我们在Login.cshtml中做登录的
```
@{
    Layout = null;
}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>登录</title>
    <link rel="stylesheet" href="~/Content/bootstrap.min.css">
    <link rel="stylesheet" href="~/Content/login.css">
</head>
<body>
    <video id="v1" autoplay loop muted>
        <source src="~/imgs/jing.mp4" />
    </video>
    <section>
        <div class="box">
            <div class="alert alert-danger alert-dismissible fade show invisible text-center"
                 style="width: 25rem; margin-top: -8rem;" role="alert">
                <strong>提示：</strong><label for="" id="msg" style="height: 0.5rem; width: 9rem;">555</label>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="container">

                <div class="form">
                    <h2>微说说</h2>
                    <form id="loginForm" action="/loginDone" method="POST">

                        <div class="inputBox">
                            <input type="text" id="username" name="username" placeholder="账号">

                        </div>
                        <div class="inputBox">
                            <input type="password" id="password" name="password" placeholder="密码">

                        </div>
                        <div class="form-group form-check row ">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">记住我</label>
                        </div>
                        <div class="inputBox">
                            <button type="button" class="btn btn-outline-primary" onclick="jump()">登录</button>
                            <button type="button" class="btn btn-outline-primary" onclick="register()">注册</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script src="~/Scripts/jquery-3.6.0.min.js"></script>
    <script src="~/Scripts/bootstrap.min.js"></script>
    <script>
        function register() {
            window.location.href = '/users/register'
        }
        function jump() {
            let username = $('#username').val();
            let password = $('#password').val();
            if (username.length > 0 && password.length > 0) {
                $.ajax({
                    url: "/users/loginDone",
                    type: "POST",
                    data: $('#loginForm').serialize(),
                    dataType: "json",
                    success: function (res) {
                        console.log(res);
                        if (res.code === 200) {
                            window.location.href = "/Messages/Index";
                        } else {
                            alert_fn(res.msg);
                        }
                    },
                    error: function (msg) {
                        alert_fn('网络或者服务器有问题')
                    }
                })
            } else {
                alert_fn('用户和密码不能为空');
                return false;
            }
        }
        function alert_fn(msg) {
            msg = msg || '密码不能为空，两次密码应该一致';
            $('#msg').text(msg);

            $('.invisible').removeClass('invisible');

            setTimeout(() => {
                $('.alert').addClass('invisible');
            }, 3000)
        }
    </script>
</body>
</html>

```
#### 接下来我们开始登录注册的代码，我们在UsersController.cs中
```
        public string registerDone(RegisterModel registerModel)
        {
            var username = registerModel.Username.Trim();
            var password = registerModel.Password.Trim();
            var confirmPassword = registerModel.ConfirmPassword.Trim();

            dynamic result;


            if (username.Length > 0 && password.Trim().Length > 0 && password.Equals(confirmPassword))
            {
                var user = db.Users.Where(x => x.Username.Equals(username)).FirstOrDefault();

 
                if (user != null)
                {
                    result = new
                    {
                        code = 1000,
                        msg = "当前用户名已经存在"
                    };
                }
                else
                {
                    db.Users.Add(new Users
                    {
                        Username = username,
                        Password = password,
                        CreatedAt =  DateTime.Now,
                        UpdatedAt = DateTime.Now,
                        
                        
                    });

                    db.SaveChanges();


                    result = new
                    {
                        code = 200,
                        msg = "注册成功"
                    };
                }


            }
            else
            {
                result = new
                {
                    code = 1000,
                    msg = "用户名密码不能为空，并且两次密码应该一致，请核对后重试!=_="
                };
            }


            return JsonConvert.SerializeObject(result);
        }

        public string LoginDone(LoginModel login)
        {
            dynamic result;

            if (login.Username.Trim().Length > 0 && login.Password.Trim().Length > 0)
            {
                var user = db.Users.Where(x => x.Username.Equals(login.Username.Trim()) && x.Password.Equals(login.Password.Trim())).FirstOrDefault();

                if (user != null)
                {
                    result = new
                    {
                        code = 200,
                        msg = "登录成功"
                    };
                }
                else
                {
                    result = new
                    {
                        code = 1000,
                        msg = "用户名或密码不正确，请核对后重试"
                    };
                }
            }
            else
            {
                result = new
                {
                    code = 1000,
                    msg = "用户名密码不能为空，请核对后重试=_="
                };
            }

            return JsonConvert.SerializeObject(result);
        }
```