# 05-18笔记

## MVC主页搭建加入数据库

### 1. 关于连接数据库Database.SetInitializer的几种参数

```
//数据库不存在时重新创建数据库
Database.SetInitializer(new CreateDatabaseIfNotExists<SqlDbContext>());
//每次启动应用程序时创建数据库
Database.SetInitializer(new DropCreateDatabaseAlways<SqlDbContext>());
//模型更改时重新创建数据库
Database.SetInitializer(new DropCreateDatabaseIfModelChanges<SqlDbContext>());
//从不创建数据库
Database.SetInitializer<SqlDbContext>(null);

```
