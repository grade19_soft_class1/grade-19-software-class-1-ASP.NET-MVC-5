# 05-13笔记
## Views 传值 

好家伙完事后就是，哇塞（视图）的四种传值方式

### 1.ViewBag

    ViewBag是动态类型的,所以可以在控制器中直接给ViewBag添加属性赋值,实现controller向Views中传值,并且在views使用@关键字来实现使用如:

controller代码
```
public ActionResult Index()  
{  
    // 直接使用 ViewBag.name 或 ViewBag.message 的方式直接给ViewBag添加属性并赋值 ViewBag.属性名 可以自定义
    ViewBag.name = "hcj";  
    ViewBag.message = "这是ViewBag传值方式";  
    return View();  
}  
```
Views代码
```
<div>  
    @ViewBag.message   //这是ViewBag传值方式 
    @ViewBag.name      //hcj
</div>  
```

### 2.ViewData

    注：ViewData 只对当前Action有效 (只能对当前url使用(视图))

    ViewData是已键值对的形式进行添加属性赋值,实现controller向Views中传值,并且在views使用@关键字来实现使用如:

controller代码
```
public ActionResult Index()  
{  
    // 直接使用 ViewData["name"] 或 ViewData["message"] 的方式直接给ViewBag添加属性并赋值
    ViewData["name"] = "hcj";  
    ViewData["message"] = "这是ViewBag传值方式";  
    return View();  
}  
```
Views代码
```
<div>  
    @ViewData.message   //这是ViewBag传值方式 
    @ViewData.name      //hcj
</div>
```
### 3.TempData

    注：TempData 可以对任何Action有效 (可以对任何url使用(视图) 但是只能使用一次)

    TempData跟ViewData传值的方式是一样的,也是已键值对的方式进行添加属性赋值

controllers代码
```
public ActionResult Index()  
{  
    // 直接使用 ViewBag["name"] 或 ViewData["message"] 的方式直接给ViewBag添加属性并赋值
    ViewData["name"] = "hcj";  
    ViewData["message"] = "这是ViewBag传值方式";  
    return View();  
}  
```
Views代码
```
# Login.cshtml
<div>  
    @TempData.message   //这是ViewBag传值方式 
    @TempData.name      //hcj
</div>
```

### 4.Model

    Model是通过强类型绑定,实现方式进行controller向Views中进行添加赋值,因为Model是通过强类型绑定方式传值,所以你使用@model进行类型指定的时候是什么类型的值,那么Model这个对象就是什么类型的;因为在上面三种传值方式是弱类型的,所以它们的类型都是dynamic类型的

controller代码
```
public ActionResult Index()  
{  
    //我们先定义一个Student类,类里有 name 与 message 字段
    public class Student {
        public string Name {get;set;}
        public string Message {get;set;}
    }

    public class Obj {
        public List<Student> Student {get;set};
    }

    // 进行添加值 
    List<Student> students = new List<Student>();
    for (var i = 1;i <= 10; i++ ) {
        students.add(new Student{ Name = "姓名" + i, Message = "内容" + i} )
    }

    // 实例化
    Obj obj = new Obj();
    obj.Student = students;

    return View(obj);
}  
```
Views代码
```
// 有两种方法可以来实现绑定值
// 1.using + 命名空间    如果使用using方式进行绑定值那么 var为dynamic类型,也就是弱类型
// 2.@model + 命名空间   如果使用 @model 方式进行绑定值得那么 var为你绑定的类型,就是强类型


@using app.Model;
<div>
    <ul>
    @foreach ( var i in Model)
    {
        <li>@i.Message</li>
        <li>@i.Name</li>
    }
</ul>
</div>

==============================================================

如我的Obj对象是在 app.controllers.StudentController 下,我使用@model进行绑定值,就要进行强制绑定是哪个类,这就是弱类型
@model app.controllers.StudentController.Obj
<div>
    <ul>
    @foreach ( var i in Model.Student)
    {
        <li>@i.Message</li>
        <li>@i.Name</li>
    }
</ul>
</div>

```