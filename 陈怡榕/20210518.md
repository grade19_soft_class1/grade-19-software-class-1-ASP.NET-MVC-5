# 2021年05月18日 周二 小雨🌧

## 今天还是继续微说说

## 一. Linq：

1. 面向对象与数据访问两个领域长期分裂，各自为政。

2. 编程语言中的数据类型与数据库中的数据类型形成两套不同的体系，例如：

    C#中字符串用string数据类型表示。

    SQL中字符串用NVarchar/Varchar/Char数据类型表示。

3. SQL编码体验落后

    没有智能感知效果。

    没有严格意义上的强类型和类型检查。

4. SQL和XML都有各自的查询语言，而对象没有自己的查询语言。

> 上面描述的问题，都可以使用LINQ解决，那么究竟什么是LINQ呢？

    1、LINQ（Language Integrated Query）即语言集成查询。

    2、LINQ是一组语言特性和API，使得你可以使用统一的方式编写各种查询。用于保存和检索来自不同数据源的数据，从而消除了编程语言和数据库之间的不匹配，以及为不同类型的数据源提供单个查询接口。

    3、LINQ总是使用对象，因此你可以使用相同的查询语法来查询和转换XML、对象集合、SQL数据库、ADO.NET数据集以及任何其他可用的LINQ提供程序格式的数据。
    4、Linq从另一方面来说也有效地防止了Sql注入哈哈

> LINQ主要包含以下三部分：

    1、LINQ to Objects 主要负责对象的查询。

    2、LINQ to XML 主要负责XML的查询。

    3、LINQ to ADO.NET 主要负责数据库的查询。

    LINQ to SQL

    LINQ to DataSet

    LINQ to Entities
　　
## 二. Lambda表达式：

1. 简化了匿名委托的使用。

2. Lambda让我们的代码更加的简介与方便，可以方便的用Where（）、Select（）等扩展方法对集合进行筛选，组合


## 三 . 看看例子
1. 两者 简单的语法
```
    static void Main(string[] args)
        {
            int[] scores = { 90, 71, 82, 93, 75, 81 };
            //查大于80分的成绩
            var result = from s in scores
                where s > 80
                select s;   //Linq写法
                         
            var result1 = scores.Where(s => s > 80);   //lambda表达式写法
        }
        
        Linq基本语法： from  s  in  数据源  
            where  条件语句 
            select  要查的结果
        s 是随便起的一个名字，代表这个数据源
        
        Lambda基本语法：
        所有的lambda表达式都是用新的lambda运算符 " => ",可以叫他，“转到”或者 “成为”。
        运算符将表达式分为两部分，左边指定输入参数
        右边是lambda的主体，一些筛选语句等等，可以简单的理解为sql里面的where条件
```

2. 简单的排序
```
    static void Main(string[] args)
        {
            int[] scores = { 90, 71, 82, 93, 75, 81 };
            //查大于80分的成绩
            var result = from s in scores
                where s > 80 
                orderby s descending //倒序  正序为ascending
                select s;
            var result1 = scores.OrderByDescending(s=>s).Where(s => s > 80);//倒序
            var result1 = scores.OrderBy(s=>s).Where(s => s > 80);//正序
        }
```

3. 简单的函数计算
```
    int[] scores = { 90, 71, 82, 93, 75, 82 };
        //查大于80分的成绩和
        var result = (from s in scores
                where s > 80 
                select s).Sum();
        var result1 = scores.Where(s => s > 80).Sum();

```



